module.exports = {
    env: {
        browser: true,
        es2021: true,
    },
    extends: "airbnb-base",
    overrides: [
    ],
    parserOptions: {
        ecmaVersion: "latest",
        sourceType: "module",
    },
    rules: {
        indent: ["error", 0],
        quotes: ["error", "double", 0],
        semi: ["error", "always", 0],
        "no-shadow": ["error", 0],
        expressions: ["error", 0],
    },
};
