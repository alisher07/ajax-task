import '../styles/style.css';
import createCommunity from './community.js';
// eslint-disable-next-line import/extensions
import { JoinUsSectionFactory } from './join-us-section.js';

const factorySection = new JoinUsSectionFactory();

document.addEventListener('DOMContentLoaded', () => {
  createCommunity();
  return factorySection.create('standard');
  
});
