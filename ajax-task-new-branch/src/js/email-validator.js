const validEmailEndings = ['gmail.com', 'outlook.com'];

function validate(email) {
  const emailParts = email.split('@');
  if (emailParts.length !== 2) {
    return false;
  }
  const emailEnding = emailParts[1];
  return validEmailEndings.includes(emailEnding);
}

export {validate};
