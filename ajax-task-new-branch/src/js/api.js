const request = (url, formData) => {
    // Send a POST Ajax request to the server
    return fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    }).then((response) => {
      if (!response.ok) {
        return response.json().then((errResData) => {
          const error = new Error(`${response.status}: ${response.statusText}`);
          error.data = errResData;
          throw error;
        });
      }
      return response.json();
    });
  };
  
  export const subscribeRequest = (emailInput) => {
      return request("http://localhost:8080/subscribe", {
          email: emailInput,
      });
  }
  
  export const unsubscribeRequest = (emailInput) => {
      return request("http://localhost:8080/unsubscribe", {
          email: emailInput,
      });
  }
