function createCommunity() {
  var communitySection = document.querySelector("#community-section");
  communitySection.className = "app-section";
  var title = document.createElement("h2");
  title.className= "app-title";
  title.innerHTML="Big Community of<br />People Like You";
  communitySection.appendChild(title);
  var subtitle = document.createElement("p");
  subtitle.className="app-subtitle";
  subtitle.innerHTML="We’re proud of our products, and we’re really excited<br />when we get feedback from our users."
  communitySection.appendChild(subtitle)
  var communityList = document.createElement("ul");
  communityList.className = "community-list";
  communitySection.appendChild(communityList);

  return fetch("http://localhost:8080/community")
    .then((response) => { 
      if (!response.ok) {
        return response.json().then((errResData) => {
          const error = new Error(`${response.status}: ${response.statusText}`);
          error.data = errResData;
          throw error;
        });
      }
      return response.json();
    })
    .then((person) => {
      console.log(person);
      for (let i = 0; i < 3; i++) {
        const card = document.createElement("li");
      communityList.appendChild(card);
      card.className = "card";
      const cardImg = document.createElement("img");
      cardImg.className = "card__avatar";
      cardImg.src = person[i].avatar;
      card.appendChild(cardImg);
      const cardText = document.createElement("p");
      cardText.className = "card__text";
      cardText.textContent = person[i].text;
      card.appendChild(cardText);
      const cardName = document.createElement("h4");
      cardName.className = "card__fullname";
      cardName.textContent = `${person[i].firstName} ${person[i].lastName}`;
      card.appendChild(cardName);
      const cardPosition = document.createElement("h5");
      cardPosition.className = "card__position";
      cardPosition.textContent = person[i].position;
      card.appendChild(cardPosition);
      }
    });
}

export default createCommunity;