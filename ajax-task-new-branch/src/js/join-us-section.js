import { validate } from './email-validator.js';
import './api.js';
class JoinUsSection {
  constructor(title, emailButton) {
    this.title = title;
    this.emailButton = emailButton;
  }

  createSection() {
    const joinProgramSection = document.querySelector('.join-program-section');

    const layer = document.createElement('div');
    layer.style.background = 'url(./assets/images/join-our-programm-background.png)';
    layer.style.height = '100%';
    layer.style.width = '100%';
    layer.style.display = 'flex';
    layer.style.alignItems = 'center';
    layer.style.justifyContent = 'center';
    layer.style.flexDirection = 'column';
    layer.style.gap = '43px';

    joinProgramSection.appendChild(layer);

    const title = document.createElement('h2');
    title.innerHTML = this.title;
    title.classList.add('join-program__article-title');
    title.style.color = 'rgba(255, 255, 255, 1)';
    title.classList.add('app-title');

    const subtitle = document.createElement('h3');
    subtitle.classList.add('join-program__article-subtitle');
    subtitle.innerHTML = 'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';
    subtitle.style.color = 'rgba(255, 255, 255, 0.7)';
    subtitle.style.maxWidth = '350px';
    subtitle.classList.add('app-subtitle');

    const emailFrom = document.createElement('form');
    emailFrom.classList.add('email-form');
    emailFrom.style.marginBlockEnd = '70px';

    const emailInput = document.createElement('input');
    emailInput.type = 'email';
    emailInput.placeholder = 'Email';
    emailInput.classList.add('email-input');
    emailInput.style.opacity = '0.5';
    emailInput.style.width = '400px';
    emailInput.style.height = '36px';
    emailInput.value = localStorage.getItem('email');

    const emailButton = document.createElement('button');
    emailButton.classList.add('app-section__button', 'email-input__button--subscribe');
    emailButton.type = 'submit';
    emailButton.innerHTML = this.emailButton;
    emailButton.style.borderRadius = '18px';
    emailButton.style.width = '111px';
    emailButton.style.height = '36px';
    emailButton.style.marginLeft = '25px';

    emailFrom.appendChild(emailInput);
    emailFrom.appendChild(emailButton);

    layer.appendChild(title);
    layer.appendChild(subtitle);
    layer.appendChild(emailFrom);

const form = document.querySelector('form');
form.addEventListener('submit', (e) => {
  e.preventDefault();
  const email = emailInput.value.trim();
  const isValidEmail = validate(email);

  if (isValidEmail) {
    localStorage.setItem('email', email);
    emailInput.style.display = 'none';
    emailButton.innerHTML = 'Unsubscribe';
    emailButton.classList.remove('email-input__button--subscribe');
    emailButton.classList.add('email-input__button--unsubscribe');
  }

});

emailButton.addEventListener('click', () => {
  if (emailButton.innerHTML === 'Unsubscribe') {
    localStorage.removeItem('email');
    emailInput.value = '';
    emailInput.style.display = 'block';
    emailButton.innerHTML = 'Subscribe';
    emailButton.classList.remove('email-input__button--unsubscribe');
    emailButton.classList.add('email-input__button--subscribe');
  }

});
}
}

export class JoinUsSectionFactory {
  create(type) {
    switch (type) {
      case 'standard':
        return new JoinUsSection('Join Our Program', 'subscribe').createSection();
      case 'advanced':
        return new JoinUsSection('Join Our Advanced Program', 'Subscribe to Advanced Program').createSection();
    }
  }
}

export default { JoinUsSection, JoinUsSectionFactory };
